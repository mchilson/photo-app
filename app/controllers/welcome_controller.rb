class WelcomeController < ApplicationController
  
  # allows the index page to not require authentication allowing it to be seen publicly
  skip_before_action :authenticate_user!, only: [:index]
  
  def index
  end
end
